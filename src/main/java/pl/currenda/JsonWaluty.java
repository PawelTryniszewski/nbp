package pl.currenda;

import java.util.ArrayList;

public class JsonWaluty {
    private Object table, ratess;
    private String code, currency;
    private double topCount;
    private Object date, startDate, endDate;
    ArrayList< Rates> rates = new ArrayList<>();

    public Object getTable() {
        return table;
    }

    public void setTable(Object table) {
        this.table = table;
    }

    public Object getRatess() {
        return ratess;
    }

    public void setRatess(Object ratess) {
        this.ratess = ratess;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getTopCount() {
        return topCount;
    }

    public void setTopCount(double topCount) {
        this.topCount = topCount;
    }

    public Object getDate() {
        return date;
    }

    public void setDate(Object date) {
        this.date = date;
    }

    public Object getStartDate() {
        return startDate;
    }

    public void setStartDate(Object startDate) {
        this.startDate = startDate;
    }

    public Object getEndDate() {
        return endDate;
    }

    public void setEndDate(Object endDate) {
        this.endDate = endDate;
    }

    public ArrayList<Rates> getRates() {
        return rates;
    }

    public void setRates(ArrayList<Rates> rates) {
        this.rates = rates;
    }
}
