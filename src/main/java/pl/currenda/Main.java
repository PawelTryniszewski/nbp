package pl.currenda;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String waluta,data1,data2,czyKoniec;

        do {
            System.out.println("Choose EUR, GBP, CHF, USD");
            waluta = scanner.next().toLowerCase();
            System.out.println("Put start date in yyyy-MM-dd format");
            data1 = scanner.next().toLowerCase();
            System.out.println("Put end date in yyyy-MM-dd format");
            data2 = scanner.next().toLowerCase();
            try {
                URL url = new URL("http://api.nbp.pl/api/exchangerates/rates/c/"+waluta+"/"+data1+"/"+data2+"/");
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    JsonWaluty walutyJson = new Gson().fromJson(inputLine, JsonWaluty.class);
                    System.out.println(walutyJson.getCode());
                    System.out.println(walutyJson.getCurrency());
                    List<Rates> ratesList = walutyJson.getRates();
                    float aBid =0;
                    float aAsk =0;
                    for (int i = 0; i < ratesList.size(); i++) {
                        aBid+=walutyJson.getRates().get(i).getBid();
                        aAsk+=walutyJson.getRates().get(i).getAsk();
                    }
                    float sredniaBid = aBid/ratesList.size();
                    float sredniaAsk = aAsk/ratesList.size();
                    double b =0;
                    for (int i = 0; i < ratesList.size(); i++) {
                        b+=Math.pow((ratesList.get(i).getAsk()-sredniaAsk),2);
                    }
                    b/=ratesList.size();
                    System.out.printf("Odchylenie standardowe kursów sprzedaży %.4f\n",Math.sqrt(b));
                    System.out.printf("średni kurs kupna %.4f\n",sredniaBid);
                }
                in.close();
            } catch (IOException e) {
                System.out.println("Wrong format.");
            }
            System.out.println("Exit? yes/no");

            czyKoniec = scanner.next().toLowerCase();
        }while(!czyKoniec.equals("yes"));

    }
}
