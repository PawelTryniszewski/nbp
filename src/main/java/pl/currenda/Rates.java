package pl.currenda;

public class Rates {
    private String no;
    private String effectiveDate;
    private float mid;
    private float bid;
    private float ask;

    public String getNo() {
        return no;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public float getMid() {
        return mid;
    }

    public float getBid() {
        return bid;
    }

    public float getAsk() {
        return ask;
    }
}
